#!/usr/bin/env python

# Modules
import json
import os
import glob
import csv
import stashy
import urllib3
from git import Repo
import getpass
import requests
import urllib.request
import time

# Désactivation du message d'alerte certificat autosigné
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Variable générique
score_approved_total = 0
total_PR = 0
trying = False


def gitdljson(path):
    boucle = True
    nbr_list = 0
    username = input("Nom d'utilisateur: ")
    password = getpass.getpass('Mot de passe: ')
    if not password:
        print("Vous n'avez pas mis de mot de passe.")
    trying = False
    while trying is not True:
        host = input("URL de login: ")
        host = str(host) # Securisation de l'input pour la CWE-22
        try:
            if urllib.request.urlopen(host).getcode() == 200:
                trying = True
                print("Site accessible, connexion...")
            else:
                print("Site injoignable, avez-vous renseigné la bonne URL?")
        except urllib.error.HTTPError:
            print("Site injoignable, veuillez recommencer...\n")
        except urllib.error.URLError:
            print("L'url est incorrect, veuillez recommencer...\n")
        host = host.replace("https://", "")
        host = host.replace("http://", "")
        host = host.replace("/", "")

    SSL = input("Certificat auto-signé: [O/N] ")

    if SSL == "O":
        SSLverify = False
        print("Il est recommandé d'ajouter ou de ne pas utiliser de certificat autosigné.")
    elif SSL == "N":
        SSLverify = True
    else:
        print("Vous n'avez pas répondu correctement.")
        SSLverify = True

    trying = False
    while trying is not True:
        try:
            bitbucket = stashy.connect(
                "http://" + host + "/",
                username,
                password,
                verify=SSLverify)
            trying = True
        except requests.exceptions.ProxyError:
            print("Il semble que vous utilisiez un proxy et que le terminal ne soit pas configuré.\nVeuillez fournir les informations au terminal.")
            exit()

    project = input("Nom du projet: ")
    while boucle is not False:
        try:
            repos = bitbucket.projects[project].repos.list()
            json_value = repos[nbr_list]
            json_value_name = json_value['slug']
            print("Téléchargement de "  + json_value_name + "...")
            log = json_value_name + ".git"
            url = "http://" + username + "@" + host + "/rest/api/1.0/projects/" + project + "/repos/" + json_value_name + "/pull-requests/?state=MERGED&limit=200"
            response = requests.get(url, auth=(username, password), verify=SSLverify)
            data = response.text
            file = open(path + json_value_name + ".json", "w")
            try:
                file.writelines(data)
            except:
                pass
            nbr_list = nbr_list + 1
        except IndexError:
            print("Erreur d'index")
            boucle = False
        except requests.exceptions.ProxyError:
            print("Il semble que vous utilisiez un proxy et que le terminal ne soit pas configuré.\nVeuillez fournir les informations au terminal.")
            exit()
            boucle = False
        except stashy.errors.AuthenticationException:
            print("Mauvais identifiants, veuillez recommencer.")
            username = input("Nom d'utilisateur: ")
            password = getpass.getpass('Mot de passe: ')
            if not password:
                print("Vous n'avez pas mis de mot de passe.")
            bitbucket = stashy.connect(
                "http://" + host + "/",
                username,
                password,
                verify=SSLverify)

# Retour input du chemin du dossier à analyser
print("~Merci d'utiliser le script de vérification de pullRequest par Benoît B.~")
print("Veuillez renseigner le chemin complet du dossier où seront téléchargés puis analysés les .json extraits du bitbucket.")
trying = False
while trying is not True:
    path = input("Chemin complet: ")
    if path[-1] != "\\":
        path = path + "\\"
    if os.path.isdir(path) is not True:
        print("Le chemin renseigné n'est pas correct.")
    elif os.path.isdir(path) is True:
        trying = True

print("Veuillez renseigner le chemin complet du dossier où sera créer le fichier de log.")
trying = False
while trying is not True:
    pathFilelog = input("Chemin complet: ")
    if pathFilelog[-1] != "\\":
        pathFilelog = pathFilelog + "\\"
    if os.path.isdir(pathFilelog) is not True:
        print("Le chemin renseigné n'est pas correct.")
    elif os.path.isdir(pathFilelog) is True:
        resultFileFull = pathFilelog + "result.csv"
        trying = True

print("\n~Connexion au bitbucket~")
gitdljson(path)
print("~Début de l'analyse~")

# Ouverture, création et écriture des entêtes du fichier JSON
header = ['Fichier analyse', 'ID', 'state_data', 'count_approved', 'conformite', 'create_date', 'closed_date', 'Lien vers la PR']
with open(resultFileFull, 'a', newline='', encoding="utf8") as fichiercsv:
    writer = csv.writer(fichiercsv)
    writer.writerow(header)

# Début de la boucle par fichier présent dans le dossier à analyser
for filename in glob.glob(os.path.join(path, '*.json')):
    with open(os.path.join(os.getcwd(), filename), 'r') as f:
        # Variables générique à restaurer à chaque boucles
        approval_boucle = 0
        approval_boucle_error = True
        score_approved = 0
        values_boucle = 0
        values_boucle_error = True
        score_approved_PR = 0
        actual_PR = 0
        create_date_timestamp = ""
        create_date = ""
        closed_date_timestamp = ""
        closed_date = ""
        url_PR = ""
        repo_name = ""
        project_name = ""

        print("\nOuverture de " + filename)

        # Ouverture et retour du fichier JSON
        json_read = open(filename)
        try:
            data = json.load(json_read)
        except json.decoder.JSONDecodeError:
            pass

        # Boucle du début d'analyser du fichier JSON pour l'id et l'état
        while values_boucle_error is not False:
            try:
                # Get id
                id_data = data["values"][values_boucle]["id"]

                # Get state
                state_data = data["values"][values_boucle]["state"]
                if state_data == "MERGED":
                    # Si état MERGED alors boucle pour récupérer tous les APPROVED du fichier JSON
                    while approval_boucle_error is not False:
                        try:
                            result = data["values"][values_boucle]["reviewers"][approval_boucle]["approved"]
                            if result is True:
                                Name = data["values"][values_boucle]["reviewers"][approval_boucle]["user"]["displayName"]
                                score_approved = score_approved + 1
                            elif result is False:
                                Name = data["values"][values_boucle]["reviewers"][approval_boucle]["user"]["displayName"]
                        except IndexError:
                            approval_boucle_error = False
                        approval_boucle = approval_boucle + 1
                    print("Le PR " + str(id_data) + " a été " + state_data + " et approuvé " + str(score_approved) + " fois.")

                    if (state_data == "MERGED") and score_approved >= 2:
                        print("Le PR " + str(id_data) + " est CONFORME.")
                        score_approved_total = score_approved_total + 1
                        score_approved_PR = score_approved_PR + 1
                        conform = "CONFORME"
                    elif (state_data != "MERGED") or score_approved < 2:
                        print("Le PR " + str(id_data) + " est NON CONFORME.")
                        conform = "NON CONFORME"

                # Get createdDate
                create_date_timestamp = str(data["values"][values_boucle]["createdDate"])
                #Enlever les 3 derniers chiffres
                loop_time = 0
                while loop_time<3:
                    if create_date_timestamp[len(create_date_timestamp)-1]=="0":
                        create_date_timestamp=create_date_timestamp[:-1]
                    loop_time=loop_time+1

                create_date = time.ctime(int(create_date_timestamp))
                # Get createdDate
                closed_date_timestamp = str(data["values"][values_boucle]["closedDate"])
                #Enlever les 3 derniers chiffres
                loop_time = 0
                while loop_time<3:
                    if closed_date_timestamp[len(closed_date_timestamp)-1]=="0":
                        closed_date_timestamp=closed_date_timestamp[:-1]
                    loop_time=loop_time+1
                closed_date = time.ctime(int(closed_date_timestamp))

                #Get project
                project_name = data["values"][values_boucle]["fromRef"]["repository"]["project"]["key"]
                #Get repos
                repo_name = data["values"][values_boucle]["fromRef"]["repository"]["name"]
                #URL de la PR
                url_PR = "https://scm.intranet-adsn.fr/projects/"+str(project_name)+"/repos/"+str(repo_name)+"/pull-requests/"+str(id_data)+"/overview"

                # Append des données dans le fichier CSV
                list_to_save = []
                list_to_save.append(filename)
                list_to_save.append(id_data)
                list_to_save.append(state_data)
                list_to_save.append(score_approved)
                list_to_save.append(conform)
                list_to_save.append(create_date)
                list_to_save.append(closed_date)
                list_to_save.append(url_PR)

                # Variables à restaurer de la boucle de lecture JSON
                approval_boucle = 0
                approval_boucle_error = True
                score_approved = 0

                # Ajout de valeurs sur une variable
                values_boucle = values_boucle + 1
                total_PR = total_PR + 1
                actual_PR = actual_PR + 1

                header = ['Fichier analyse', 'ID', 'state_data', 'count_approved', 'conformite', 'create_date', 'closed_date', 'Lien vers la PR']
                with open(resultFileFull, 'a', newline='', encoding="utf8") as fichiercsv:
                    writer = csv.writer(fichiercsv)
                    writer.writerow(list_to_save)

            except IndexError:
                values_boucle_error = False

        # Calcul du pourcentage de conformité par fichier avec vérification d'erreurs standard
        if actual_PR > 0:
            pourcentageConformeActuel = int(score_approved_PR) / int(actual_PR) * 100
            print("Résultat de conformité du fichier: " + str(score_approved_PR) + "/" + str(actual_PR) + " soit " + str(pourcentageConformeActuel) + "%")
            print("~--------------------------------------------------------------~")
        elif  actual_PR <= 0:
            print("Impossibilité de calculer le pourcentage de conformité du fichier.")

# Fermeture du JSON par sécurité
try:
    json_read.close()
except NameError:
    pass

# Calcul du pourcentage de conformité par fichier avec vérification d'erreurs standard
if total_PR > 0:
    pourcentageConforme = (score_approved_total / (total_PR)) * 100
    print("\nRésultat de conformité total: " + str(score_approved_total) + "/" + str(total_PR) + " soit " + str(pourcentageConforme) + "%")
elif total_PR <= 0:
    print("Le ou les fichiers n'ont pas d'entrées valables.")

# Ajout du résultat dans le fichier de CSV
list_final_result = []
list_final_result.append(str(score_approved_total) + "/" + str(total_PR))
resultFileFull = pathFilelog + "\\result.csv"
with open(resultFileFull, 'a', newline='', encoding="utf8") as fichiercsv:
    writer = csv.writer(fichiercsv)
    writer.writerow(list_final_result)
